var MongoClient = require('mongodb').MongoClient;
var db;

MongoClient.connect('mongodb://localhost:27017/myWebsite',function (err, database) {
    if(err) throw err;
    db = database;
    console.log(">>> Connected to Database");
});

module.exports = {collection : (name) => db.collection(name)};
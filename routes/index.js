var express = require('express');
var router = express.Router();
var db = require('./db');

// allowing access from localhost
router.use(function (req, res, next){    
    
    
    //res.header("Access-Control-Allow-Origin", "http://localhost");// uncomment for devlopment
    //res.header("Access-Control-Allow-Origin", "http://prasannashiwakoti.com"); // comment for devlopment
    res.header("Access-Control-Allow-Origin", "*");// just for testing
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();    
});

// getting all projects
router.get('/projects', function(req, res) {  
  
  db.collection('myProjects').find().toArray(function (err, projects){
      
        if(err) return res.status(500).send({msg : "Error : Unable to get project."});
        
        res.status(200).send(projects);      
  });    
});

// getting about me 
router.get('/about', function(req, res) {  

  db.collection('aboutMe').find().toArray(function (err, contents){
      
        if(err) return res.status(500).send({msg : "Error : Unable to get contents for about."});
        
        res.status(200).send(contents);      
  });  
});

module.exports = router;
